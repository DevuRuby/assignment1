class MarksController < ApplicationController
	def index
		@marks = Mark.all
	end

	def show
		@mark = Mark.find(params[:id])
	end

	def new
		@mark = Mark.new
	end

	def create
		respond_to do |f|
			@mark = Mark.new(marks_params)
			if @mark.save
				f.html { redirect_to @mark, notice: 'Mark was successfully added' }
			else
				render 'new'
			end
		end
	end

	def weighted_averg
    @student = Stud.find_by(params[:stud_id])
    @marks = Mark.where(stud_id: @student.id)
    @avg = Mark.avg(@marks)
   end

	private
	def marks_params
		params.require(:mark).permit(:marks, :stud_id, :subject_id)
	end
end
