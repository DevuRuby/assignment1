class StudsController < ApplicationController
	def index
		@students = Stud.all
	end

  def show
 		@student = Stud.find(params[:id])
  end

	def new
		@student = Stud.new
  end

  def create
     
    respond_to do |f|
    	@student = Stud.new(student_params)
   		if @student.save
    	 # f.html { redirect_to @student, notice: 'Student was successfully added' }
       @students = Stud.all
       f.js{ }
      else
    	 render "new"
    	end
    end
  end

  private
  def student_params
  	params.require(:stud).permit(:name, :email)
  end

end

     