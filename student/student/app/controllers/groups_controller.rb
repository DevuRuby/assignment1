class GroupsController < ApplicationController
	def index
		@groups = Group.all
	end

	def show
		@group = Group.find(params[:id])	
	end

	def new
		@group = Group.new
	end

	def create
		respond_to do |f|
			@group = Group.new(group_params)
			if @group.save
				f.html { redirect_to @group, notice: 'Group was successfully added' }
			else
				render 'new'
			end
		end
	end

	def add_student
		if params[:group_id]
			@group =Group.find(params[:group_id])
			@addstudent = Stud.find_by_id(params[:stud][:id])
			@group.studs << @addstudent
	    redirect_to :action => 'index'
	  end
	end

	private
	def group_params
		params.require(:group).permit(:name)
	end
end
