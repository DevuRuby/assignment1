require 'spec_helper'

RSpec.describe MarksController, type: :model do
it 'calculate weighted average' do
subject1 = FactoryGirl.create(:subject1)
subject2 = FactoryGirl.create(:subject2)
mark1 = FactoryGirl.create(:mark1)
mark2 = FactoryGirl.create(:mark2)
marks = Mark.where stud_id: 2
@avg = Mark.avg(marks)
expect(@avg).to eq(7)
end
end
