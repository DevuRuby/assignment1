FactoryGirl.define do
	factory :mark1, class: Mark do
	  marks 45
	  stud_id 2 
	  subject_id 1
  end
  
  factory :mark2, class: Mark do
  	marks 42
  	stud_id 2
  	subject_id 2
  end
end
