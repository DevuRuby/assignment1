FactoryGirl.define do
	factory :student, class: Stud do
		sequence(:name) { |n| "name#{n}" }
		sequence(:email) { |n| "email#{n}@example.com" }
  end

  factory :student1, class: Stud do
  	name 'Anjaly'
  	email 'anjly@gmail.com'
  end
end


