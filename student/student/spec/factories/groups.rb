FactoryGirl.define do
	factory :group1, class: Group do
		sequence(:name) { |n| "name#{n}" }
  end

  factory :group2, class: Group do
  	name nil
  end
end
