require 'spec_helper'
RSpec.describe GroupsController, type: :controller do
	
	before(:each) do
   @group = FactoryGirl.create(:group1)
  end

  it 'get index template' do
  	get :index
  	expect(response.status).to eq(302)
  end

  it 'get new template' do
  	get :new
  	expect(response.status).to eq(302)
  end

   it 'render the show' do
	   expect{ (get :show, id: @stud.id).response.status.to render_template('show')}
  end

  it 'add student to group' do
     if @group.id
      id = @group.id
      @student = FactoryGirl.create(:student1)
      @group.studs << @student
      expect(response.status).to eq(200)
     end
  end
 end


