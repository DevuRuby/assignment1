require 'spec_helper'
RSpec.describe StudsController, type: :controller do
	
	before(:each) do
   @stud = FactoryGirl.create(:student)
  end

  it 'get index template' do
  	get :index
  	expect(response.status).to eq(302)
  end

  it 'get new template' do
  	get :new
  	expect(response.status).to eq(302)
  end

   it 'render the show' do
	   expect{ (get :show, id: @stud.id).response.status.to render_template('show')}
  end
 end














