Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 resources :studs, only: [:show, :create, :update, :destroy, :index,:new]
 resources :groups,  only: [:show, :create, :update, :destroy, :index,:new] do
 	post 'groups/add_student' => 'groups#add_student' , :as => :add_student
 end
 resources :marks do
 	get 'marks/weighted_averg' => 'marks#weighted_averg' , :as => :weighted_averg
 end
 root "studs#index"
end



 #     resources :teams, only: [:show, :create, :update, :destroy, :index,:new] do
 #   post 'teams/addemployee' => 'teams#addemployee' , :as => :addemployee_team
 # end