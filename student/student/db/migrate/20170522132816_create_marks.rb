class CreateMarks < ActiveRecord::Migration[5.0]
  def change
    create_table :marks do |t|
      t.integer :marks
      t.references :stud, foreign_key: true
      t.references :subjects , foreign_key: true

      t.timestamps
    end
  end
end
